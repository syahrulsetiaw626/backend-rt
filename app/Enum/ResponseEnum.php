<?php 
  
namespace App\Enum;

enum ResponseEnum:string
{
  case STORE_SUCCESS = "Sukses input data baru";
  case UPDATE_SUCCESS = "Sukses perbarui data";
  case DELETE_SUCCESS = "Sukses hapus data";
  case READ_SUCCESS = "Sukses ambil data";
  
  case STORE_FAILED = "Gagal Input data baru";
  case UPDATE_FAILED = "Gagal Perbarui data";
  case DELETE_FAILED = "Gagal Hapus data";
  case READ_FAILED = "Gagal ambil data";
  
  case ACCESS_DENIED = "Kamu tidak memiliki izin untuk akses data ini";
  case LOGIN_FAILED = "Gagal Login. Token yang anda kirim salah";
}