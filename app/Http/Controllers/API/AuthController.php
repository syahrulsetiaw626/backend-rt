<?php

namespace App\Http\Controllers\API;

use App\Enum\ResponseEnum;
use App\Http\Controllers\Controller;
use App\Models\Inhabitant;
use App\Models\User;
use App\Service\PermissionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
     /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth("api")->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        (new PermissionService())->checkToken();
        $input['user'] = auth("api")->user();
        $input['inhabitant'] = auth("api")->user()->inhabitant;
        $input['role'] = auth("api")->user()->roles;
        return response()->json($input, 200);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function register(Request $request){
      //set validation
        $validator = Validator::make($request->all(), [
            'name'      => 'required',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|min:8|confirmed'
        ]);

        //if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //create user
        $user = User::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => Hash::make($request->password)
        ]);

        if($user) {

            $token = auth("api")->attempt([
              "email" => $user->email,
              "password" => $request->password
            ]);

            return response()->json([
                'message' => 'success register',
                'user'    => $user, 
                'access_token' => $token,
                'token_type' => 'bearer', 
            ], 201);
        }

        //return JSON process insert failed 
        return response()->json([
            'message' => 'failed Register',
        ], 500);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'user' => auth("api")->user()
            // 'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function update(Request $request){
        (new PermissionService())->checkToken();

        $input = $request->validate([
          'name' => 'string',
          'email' => 'email|unique:users,email',
          'nik' => 'string',
          'phone' => 'string',
          'place_of_birth' => 'string',
          'date_of_birth' => 'date',
          'family_group_id' => 'integer',
          'address' => 'string',
          'photo' => 'string',
          'rekening_number' => 'string',
          'is_nomads' => 'boolean',
          'is_pemilu' => 'boolean'
        ]);

        // create data warga
        $inhabitant = Inhabitant::findOrFail(auth("api")->user()->inhabitant->id);
        $inhabitant->update($input);

        if(isset($input['name']) || isset($input['email'])){
          $user = User::find(auth("api")->id());
          $user->update($input);
        }

        $response['message'] = ResponseEnum::UPDATE_SUCCESS->value;
          
        return response()->json($response, 200);
    }
}
