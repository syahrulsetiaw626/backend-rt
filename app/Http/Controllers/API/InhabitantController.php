<?php

namespace App\Http\Controllers\API;

use App\Enum\GeneralEnum;
use App\Enum\ResponseEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\InhabitantRequest;
use App\Models\Inhabitant;
use App\Models\User;
use App\Service\PermissionService;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class InhabitantController extends Controller
{
    public function __construct()
    {
        //checkToken
        (new PermissionService())->checkToken();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
          // check permission
          (new PermissionService())->checkUserHasPermission('read warga');

          // create data warga
          $inhabitant = Inhabitant::paginate(10);

          $response['message'] = ResponseEnum::READ_SUCCESS->value;
          $response['data'] = $inhabitant;
          
          return response()->json($response, 200);
        } catch (QueryException $e) {
          $response['message'] = ResponseEnum::READ_FAILED->value;
          $response['error'] = $e;
          return response()->json($response, $e->getCode());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InhabitantRequest $request)
    {
        try {
          // check permission
          (new PermissionService())->checkUserHasPermission('create warga');

          $input = $request->all();
          $input['password'] = Hash::make(GeneralEnum::DEFAULT_PASSWORD->value);

          // create user and assign to 'warga'
          $user = User::create($input);
          $user->assignRole('warga');
          $input['user_id'] = $user->id;

          // create data warga
          Inhabitant::create($input);

          $response['message'] = ResponseEnum::STORE_SUCCESS->value;
          $response['data'] = [
            'user' => $user,
            'inhabitant' => $user->inhabitant
          ];
          
          return response()->json($response, 200);
        } catch (QueryException $e) {
          $response['message'] = ResponseEnum::STORE_FAILED->value;
          $response['error'] = $e;
          return response()->json($response, $e->getCode());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
          // check permission
          (new PermissionService())->checkUserHasPermission('read warga');

          // create data warga
          $inhabitant = Inhabitant::find($id);

          $response['message'] = ResponseEnum::READ_SUCCESS->value;
          $response['data'] = [
            'inhabitant' => $inhabitant,
            'user' => $inhabitant->user
          ];
          
          return response()->json($response, 200);
        } catch (QueryException $e) {
          $response['message'] = ResponseEnum::READ_FAILED->value;
          $response['error'] = $e;
          return response()->json($response, $e->getCode());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

          $input = $request->validate([
            'name' => 'string',
            'email' => 'email|unique:users,email',
            'nik' => 'string',
            'phone' => 'string',
            'place_of_birth' => 'string',
            'date_of_birth' => 'date',
            'family_group_id' => 'integer',
            'address' => 'string',
            'photo' => 'string',
            'rekening_number' => 'string',
            'is_nomads' => 'boolean',
            'is_pemilu' => 'boolean',
            'role' => 'array'
          ]);

          // check permission
          (new PermissionService())->checkUserHasPermission('update warga');

          // create data warga
          $inhabitant = Inhabitant::findOrFail($id);
          $inhabitant->update($input);

          if(isset($input['name']) || isset($input['email'])){
            $user = User::find($inhabitant->user_id);
            $user->update($input);
          }

          if(isset($input['role'])){
            $user = User::find($inhabitant->user_id);
            $user->syncRoles($input['role']);
          }

          $response['message'] = ResponseEnum::UPDATE_SUCCESS->value;
          
          return response()->json($response, 200);
        } catch (QueryException $e) {
          $response['message'] = ResponseEnum::UPDATE_FAILED->value;
          $response['error'] = $e;
          return response()->json($response, $e->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {          
          // check permission
          (new PermissionService())->checkUserHasPermission('delete warga');

          // create data warga
          $inhabitant = Inhabitant::find($id);
          User::find($inhabitant->user_id)->delete();
          $inhabitant->delete();

          $response['message'] = ResponseEnum::DELETE_SUCCESS->value;
          
          return response()->json($response, 200);
        } catch (QueryException $e) {
          $response['message'] = ResponseEnum::DELETE_FAILED->value;
          $response['error'] = $e;
          return response()->json($response, $e->getCode());
        }
    }
}
