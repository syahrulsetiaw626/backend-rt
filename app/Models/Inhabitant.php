<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inhabitant extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
      'user_id', 'nik', 'phone', 'place_of_birth', 'date_of_birth',
      'family_group_id', 'address', 'photo', 'rekening_number', 'is_nomads', 'is_pemilu'
    ];

    public function user(){
      return $this->hasOne(User::class, 'id', 'user_id');
    }
}
