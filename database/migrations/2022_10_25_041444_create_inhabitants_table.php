<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inhabitants', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('nik');
            $table->string('phone');
            $table->string('place_of_birth');
            $table->datetime('date_of_birth');
            $table->integer('family_group_id')->nullable();
            $table->string('address');
            $table->string('photo')->nullable();
            $table->string('rekening_number')->nullable();
            $table->boolean('is_nomads')->default(0);
            $table->boolean('is_pemilu')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inhabitants');
    }
};
