<?php

namespace Database\Seeders;

use App\Enum\GeneralEnum;
use App\Models\Inhabitant;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $input = [
          "name" => "admin",
          "email" => "admin",
          "nik" => "--",
          "phone" => "--",
          "place_of_birth" => "--",
          "date_of_birth" => "2000-01-01",
          "address" => "--"
        ];
        $input['password'] = Hash::make(GeneralEnum::DEFAULT_PASSWORD->value);

        // create user and assign to 'warga'
        $user = User::create([
          'name' => $input['name'],
          'email' => $input['email'],
          'password' => $input['password'],
        ]);
        $user->assignRole('superadmin');
        $input['user_id'] = $user->id;

        // create data warga
        Inhabitant::create([
          'user_id' => $input['user_id'],
          'nik' => $input['nik'],
          'phone' => $input['phone'],
          'place_of_birth' => $input['place_of_birth'],
          'date_of_birth' => $input['date_of_birth'],
          'address' => $input['address']
        ]);

        $faker = Faker::create('id_ID');
        for ($i=0; $i < 80; $i++) { 
          $input = [
            "name" => $faker->name(),
            "email" => $faker->email(),
            "nik" => "--",
            "phone" => $faker->phoneNumber(),
            "place_of_birth" => $faker->city(),
            "date_of_birth" => $faker->date(),
            "address" => $faker->address()
          ];
          $input['password'] = Hash::make(GeneralEnum::DEFAULT_PASSWORD->value);

          // create user and assign to 'warga'
          $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => $input['password'],
          ]);
          $user->assignRole('warga');
          $input['user_id'] = $user->id;

          // create data warga
          Inhabitant::create([
            'user_id' => $input['user_id'],
            'nik' => $input['nik'],
            'phone' => $input['phone'],
            'place_of_birth' => $input['place_of_birth'],
            'date_of_birth' => $input['date_of_birth'],
            'address' => $input['address']
          ]);
        }
    }
}
